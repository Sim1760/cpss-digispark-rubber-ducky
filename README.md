# Digispark Rubber Ducky
This is a project for the rubber ducky script portion of Daniel Simpson and Kaitlyn Cottrell's Cyber-Physical Systems Security term project. We're using a Digispark Attiny85 micro USB development board as a base, but it does not have a lot of memory so we'll try to use it as a dropper for our actually malicious scripts (probably a reverse shell for our AGV).

Driver: Digispark default 16.5 mhz

libs: DigiKeyboard.h
